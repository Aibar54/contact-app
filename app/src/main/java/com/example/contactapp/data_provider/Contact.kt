package com.example.contactapp.data_provider

data class Contact(
    val name: String,
    val number: String?,
    val image_uri: String?)
