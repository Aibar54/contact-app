package com.example.contactapp

import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import com.example.contactapp.databinding.ActivityMainNavBinding
import com.google.android.material.navigation.NavigationView

class MainActivity : NavigationActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainNavBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainNavBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.activityMain.toolbar)

        binding.navView.setNavigationItemSelectedListener(this)

        val toggle = ActionBarDrawerToggle(
            this, binding.drawerLayout, binding.activityMain.toolbar,
            R.string.open_nav_drawer, R.string.close_nav_drawer)

        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun closeDrawer() {
        binding.drawerLayout.close()
    }
}