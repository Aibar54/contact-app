package com.example.contactapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.contactapp.contact.ListViewFragment
import com.example.contactapp.contact.RecyclerViewFragment
import com.google.android.material.navigation.NavigationView

abstract class NavigationActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<String>
    private var currentFragment = Int.MIN_VALUE

    abstract fun closeDrawer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    Log.i("Permission: ", "Granted")
                    proceedNavigation(currentFragment)
                } else {
                    Log.i("Permission: ", "Denied")
                    showToast("Read contacts permission required for app")
                }
            }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.listViewItem -> {
                if (isPermissionGranted()) {
                    proceedNavigation(R.id.listViewItem)
                }
                else {
                    currentFragment = R.id.listViewItem
                    requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
                }
            }
            R.id.recyclerViewItem -> {
                if (isPermissionGranted())
                    proceedNavigation(R.id.recyclerViewItem)
                else {
                    currentFragment = R.id.recyclerViewItem
                    requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
                }
            }
        }
        return true
    }

    private fun proceedNavigation(fragmentId: Int) {
        if(fragmentId != currentFragment) {
            currentFragment = fragmentId
            showFragment(fragmentId)
            closeDrawer()
        } else {
            closeDrawer()
        }
    }

    private fun showFragment(fragmentId: Int) {
        val fragment: Fragment = when(fragmentId) {
            R.id.listViewItem -> ListViewFragment()
            R.id.recyclerViewItem -> RecyclerViewFragment()
            else -> return
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
    }

    private fun isPermissionGranted() =
        ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED

    private fun Context.showToast(
        message: String,
        length: Int = Toast.LENGTH_SHORT
    ) = Toast.makeText(this, message, length).show()
}