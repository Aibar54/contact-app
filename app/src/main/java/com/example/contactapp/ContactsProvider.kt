package com.example.contactapp

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.provider.ContactsContract
import com.example.contactapp.data_provider.Contact

object ContactsProvider {
    @SuppressLint("Range")
    fun getAllContacts(cr: ContentResolver): ArrayList<Contact> {
        val list = ArrayList<Contact>()
        val cur = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if ((cur?.count ?: 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                val id = cur.getString(
                    (cur.getColumnIndex(ContactsContract.Contacts._ID))
                )
                val name = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    var phoneNo: String? = null
                    if (pCur!!.moveToNext()) {
                        phoneNo = pCur.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                    }
                    pCur.close()

                    val image_uri = cur.getString(cur.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                    list.add(Contact(name, phoneNo, image_uri))
                }
            }
        }
        cur?.close()
        return list
    }
}
