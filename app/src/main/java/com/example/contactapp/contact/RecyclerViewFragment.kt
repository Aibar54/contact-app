package com.example.contactapp.contact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.contactapp.ContactsProvider
import com.example.contactapp.databinding.FragmentRecycleViewBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RecyclerViewFragment : Fragment() {

    private lateinit var binding: FragmentRecycleViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecycleViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ContactAdapterRecyclerView()
        binding.list.adapter = adapter

        lifecycleScope.launch(Dispatchers.IO) {
            val contacts = ContactsProvider.getAllContacts(requireActivity().contentResolver)
            withContext(Dispatchers.Main) {
                binding.loadingView.visibility = View.GONE
                adapter.update(contacts)
            }
        }
    }
}