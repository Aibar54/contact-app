package com.example.contactapp.contact

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.contactapp.R
import com.example.contactapp.data_provider.Contact

class ContactAdapterListView(context: Context, private val objects: MutableList<Contact>) :
    ArrayAdapter<Contact>(context, R.layout.list_item, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val convertViewCopy = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.list_item, null)

        val contactNameView = convertViewCopy.findViewById<TextView>(R.id.contact_name)
        val contactNumberView = convertViewCopy.findViewById<TextView>(R.id.contact_number)
        val imageView = convertViewCopy.findViewById<ImageView>(R.id.contact_image)

        val contact = objects[position]
        contactNameView.text = contact.name
        contactNumberView.text = contact.number
        if (contact.image_uri == null)
            imageView.setImageResource(R.drawable.ic_baseline_person_24)
        else
            imageView.setImageURI(Uri.parse(contact.image_uri))
        return convertViewCopy
    }
}