package com.example.contactapp.contact

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.R
import com.example.contactapp.data_provider.Contact
import com.example.contactapp.databinding.ListItemBinding

class ContactAdapterRecyclerView(private val items: MutableList<Contact> = mutableListOf()) :
    RecyclerView.Adapter<ContactAdapterRecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(newList: List<Contact>) {
        items.clear()
        items.addAll(newList)
        notifyItemRangeInserted(0, items.size)
    }

    class ViewHolder(val binding: ListItemBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Contact) = with(binding) {
            contactName.text = item.name
            contactNumber.text = item.number
            if(item.image_uri == null)
                contactImage.setImageResource(R.drawable.ic_baseline_person_24)
            else
                contactImage.setImageURI(Uri.parse(item.image_uri))

            root.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${contactNumber.text}")
                it.context.startActivity(intent)
            }
        }
    }
}