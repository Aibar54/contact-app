package com.example.contactapp.contact

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.contactapp.ContactsProvider
import com.example.contactapp.R
import com.example.contactapp.databinding.FragmentListViewBinding

class ListViewFragment : Fragment() {

    private lateinit var binding: FragmentListViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ContactAdapterListView(
            requireContext(), ContactsProvider.getAllContacts(requireActivity().contentResolver)
        )

        binding.list.adapter = adapter

        binding.list.setOnItemClickListener { _, itemView, _, _ ->
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${itemView.findViewById<TextView>(R.id.contact_number).text}")
            startActivity(intent)
        }
    }
}